(function() {

	app = angular.module('RentalsApp', ['ui.router', 'ui.bootstrap', 'ui.bootstrap.modal']);

	// Controlador maestro.
	//app.controller('mainCtrl', ['$scope', '$http', function($scope, $http) {}]);



	// Rutas
	app.provider('modalState', function($stateProvider) {
		var provider = this;
		this.$get = function() {
			return provider;
		}
		this.state = function(stateName, options) {
			var modalInstance;
			$stateProvider.state(stateName, {
				url: options.url,
				onEnter: function($modal, $state) {
					modalInstance = $modal.open(options);
					modalInstance.result['finally'](function() {
						modalInstance = null;
						if ($state.$current.name === stateName) {
							$state.go('^');
						}
					});
				},
				onExit: function() {
					if (modalInstance) {
						modalInstance.close();
					}
				}
			});
		};
	}).config(function($stateProvider, $urlRouterProvider,modalStateProvider) {
		$urlRouterProvider.otherwise("/biker/inicio");

		$stateProvider
			.state('biker', {
				url: '/biker',
				templateUrl: 'secciones/app.html'
			})
			.state('biker.inicio', {
				url: '/inicio',
				template: '<div>Aquiiii2</<div><div class="app-content" data-ui-view></div>'
			})
			// .state('biker.inicio.moto', {
			// 	url: '/motos/:moto',
			// 	templateUrl: 'secciones/motos.html'
			// })

		modalStateProvider.state('biker.inicio.moto', {
			url: '/motos/:moto',
			templateUrl: 'secciones/motos.html'
		});

	});



})();